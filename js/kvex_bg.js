var kvex_bg = {

    config: {},

    loginToServer: function () {




    },
    run: function () {

        this.loadConfig();
        this.loginToServer();
        this.readAllBookmarks();
    },

    sendToServer: function (input) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                // JSON.parse does not evaluate the attacker's scripts.
                console.debug(xhr.responseText);
                // var resp = JSON.parse(xhr.responseText);
                // console.debug(resp);
            }
        };
        var fullUrl = this.config.server_base_url + this.config.bookmark_url;
        console.debug(fullUrl);
        xhr.open("POST", fullUrl, true);
        this.setupHeaders(xhr);
        xhr.send(input);
    },
    setupHeaders: function (xhr) {
        xhr.setRequestHeader("Authorization", "Bearer " + );
        xhr.setRequestHeader("Content-type", "application/json");

    },
    readAllBookmarks: function () {
        chrome.bookmarks.getTree(function (results) {
            var message = JSON.stringify(results[0]);
            console.debug("#######################" + message);
            kvex_bg.sendToServer(message);
        });
    },

    loadConfig: function () {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                kvex_bg.config = JSON.parse(xhr.response);
                console.debug(kvex_bg.config);
            }
        };
        xhr.open("GET", chrome.extension.getURL('/config_resources/config.json'), true);
        xhr.send();
    }

};


//run
kvex_bg.run();