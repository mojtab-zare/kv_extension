//region {variables and functions}
const loginMessageType = "login_message";
const checkLoginType = "check_login";
const syncMessageType = "sync_message";
const logoutMessageType = "logout_message";
const saveTabsMessageType = "save_tabs_message";

function Messaging(sender, messageType, message) {
    this.sender = sender;
    this.messageType = messageType;
    this.message = message;

}

// function createKVCMessage(sender, messageType, message) {
//     var m = {};
//     m["sender"] = sender;
//     m["messageType"] = messageType;
//     m["message"] = message;
//     return m;
// }


var config = {};


function setupClientHeaders(xhr) {
    xhr.setRequestHeader("Authorization", "Basic " + btoa(config.client_id + ":" + config.client_secret));
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

}

function setupUserHeaders(xhr, accessToken) {
    xhr.setRequestHeader("Authorization", "Bearer " + accessToken);
    xhr.setRequestHeader("Content-type", "application/json");

}

function login(email, password, callbackSuccess) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            // JSON.parse does not evaluate the attacker's scripts.
            if (xhr.status === 200) {
                callbackSuccess(JSON.parse(xhr.responseText));
            } else {
                console.error("status:" + xhr.status + " response" + xhr.responseText)
            }
        }
    };
    var fullUrl = config.server_base_url + config.login_url;
    console.debug(fullUrl);
    xhr.open("POST", fullUrl, true);
    setupClientHeaders(xhr);
    var data = "grant_type=password&username=" + email + "&password=" + password;
    xhr.send(data);
    console.log("login sent");
}


const senderName = "bg";
const oauthTokenKey = "oauth_token";

function verifyAndParseJWT(token) {
    const parse = JSON.parse(token);
    return jwt_decode(parse.access_token);

}

function checkLogin(message, sender, sendResponse) {

    chrome.storage.sync.get(oauthTokenKey, function (token) {
        var response;

        if (!chrome.runtime.lastError) {

            var oauth_token = token.oauth_token;
            console.log(oauth_token);

            try {
                var parsedJWT = verifyAndParseJWT(oauth_token);
                response = new Messaging(senderName, checkLoginType, parsedJWT.user_name);
                //todo test check expired token!?...
                sendResponse(response);
            } catch (ex) {
                response = new Messaging(senderName, checkLoginType, {});
                sendResponse(response);
            }

        } else {
            response = new Messaging(senderName, checkLoginType, {});
            sendResponse(response);
        }
    });

}


function getOauthToken(callback) {
    chrome.storage.sync.get(oauthTokenKey, function (token) {
        var oauth_token = token.oauth_token;
        callback(oauth_token);
    });
}


function getFormattedMessageString(message, sender) {
    return "Message '" + JSON.stringify(message) + "' from Sender '" + sender.url + "'";
}

function loadConfig() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            config = JSON.parse(xhr.response);
            console.debug(config);
        }
    };
    xhr.open("GET", chrome.extension.getURL('/config_resources/config.json'), true);
    xhr.send();
}

function loginMessageHandler(message, sender, sendResponse) {
    const callbackSuccess = function (response) {
        const messaging = new Messaging(senderName, loginMessageType, response);


        var testPrefs = JSON.stringify(response);
        var jsonFile = {};
        jsonFile[oauthTokenKey] = testPrefs;
        chrome.storage.sync.set(jsonFile, function () {
            console.log('Saved', oauthTokenKey, testPrefs);
        });


        sendResponse(messaging);
    };
    login(message.message.email, message.message.password, callbackSuccess);
}

function syncMessageHandler(message, sender, sendResponse) {
    getOauthToken(function (token) {
        console.log(token);
        const parsedToken = JSON.parse(token);
        const accessToken = parsedToken.access_token;
        readAllBookmarks(sendResponse, accessToken);
    })
}


function readAllBookmarks(sendResponse, accessToken) {
    chrome.bookmarks.getTree(function (results) {
        var message = JSON.stringify(results[0]);
        console.debug(message);
        sendToServer(message, sendResponse, accessToken);
    });
}

function sendToServer(input, sendResponse, accessToken) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                const response = new Messaging(senderName, syncMessageType, null);
                console.debug(xhr.responseText);
                sendResponse(response);
            } else if (xhr.status === 401) {
                console.warn("need to login again!");
                chrome.storage.sync.remove(oauthTokenKey, function () {
                    const response = new Messaging(senderName, logoutMessageType, null);
                    sendResponse(response);
                });
            } else {
                console.error("status:" + xhr.status + " response" + xhr.responseText)
            }

        }
    };
    var fullUrl = config.server_base_url + config.bookmark_url;
    console.debug(fullUrl);
    xhr.open("POST", fullUrl, true);
    setupUserHeaders(xhr, accessToken);
    xhr.send(input);
}
function sendTabsToServer(input, sendResponse, accessToken) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                const response = new Messaging(senderName, saveTabsMessageType, null);
                console.debug(xhr.responseText);
                sendResponse(response);
            } else if (xhr.status === 401) {
                console.warn("need to login again!");
                chrome.storage.sync.remove(oauthTokenKey, function () {
                    const response = new Messaging(senderName, logoutMessageType, null);
                    sendResponse(response);
                });
            } else {
                console.error("status:" + xhr.status + " response" + xhr.responseText)
            }

        }
    };
    var fullUrl = config.server_base_url + config.tabs_url;
    console.debug(fullUrl);
    xhr.open("POST", fullUrl, true);
    setupUserHeaders(xhr, accessToken);
    xhr.send(input);
}

function logoutMessageHandler(message, sender, sendResponse) {
    chrome.storage.sync.remove(oauthTokenKey, function () {
        const response = new Messaging(senderName, logoutMessageType, null);
        sendResponse(response);
    });
}

function getTabs(sendResponse, accessToken) {

    var callback = function (result) {
        const input = JSON.stringify({'tabs': result});

        console.debug(input);
        sendTabsToServer(
            input,
            sendResponse,
            accessToken);
    };
    chrome.tabs.query({currentWindow: true}, callback);
}

function saveTabsMessageHandler(message, sender, sendResponse) {

    getOauthToken(function (token) {
        console.log(token);
        const parsedToken = JSON.parse(token);
        const accessToken = parsedToken.access_token;
        getTabs(sendResponse, accessToken);
    });

}



function run() {
    loadConfig();

}

//end-region


//region {calls}
run();
chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    console.log(getFormattedMessageString(message, sender));

    switch (message.messageType) {
        case checkLoginType:
            console.log(checkLoginType);
            checkLogin(message, sender, sendResponse);
            break;
        case loginMessageType:
            console.log(loginMessageType);
            loginMessageHandler(message, sender, sendResponse);
            break;
        case syncMessageType:
            console.log(syncMessageType);
            syncMessageHandler(message, sender, sendResponse);
            break;
        case logoutMessageType:
            console.log(logoutMessageType);
            logoutMessageHandler(message, sender, sendResponse);
            break;
        case saveTabsMessageType:
            console.debug(saveTabsMessageType);
            saveTabsMessageHandler(message, sender, sendResponse);
            break;
        default:
            console.warn(["undefined message", message]);
            break;
    }

//Will get called from the script where sendResponse is defined
//     sendResponse(responseObject);
    return true;
});
//end-region