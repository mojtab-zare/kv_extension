//region {variables and functions}


// const sendMessageButtonID = "send_message";
const loginButtonID = "login_button";
const syncButtonID = "sync_button";
const saveTabsButtonID = "save_tabs_button";
const logoutButtonID = "logout_button";
const sender = "popup";
const checkLoginType = "check_login";
const loginMessageType = "login_message";
const syncMessageType = "sync_message";
const saveTabsMessageType = "save_tabs_message";
const logoutMessageType = "logout_message";

function Messaging(sender, messageType, message) {
    this.sender = sender;
    this.messageType = messageType;
    this.message = message;

}


function checkLogin() {
    var messageObject = new Messaging(sender, checkLoginType);
    chrome.runtime.sendMessage(messageObject, checkLoginCallback);

}


function isEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
}

function checkLoginCallback(response) {
    console.log(response);
    if (isEmptyObject(response.message)) {
        document.getElementById("login").classList.remove("toggle_display");
        document.getElementById("info").classList.add("toggle_display");
        console.debug("hide info show login");
    } else {
        document.getElementById("login").classList.add("toggle_display");
        document.getElementById("info").classList.remove("toggle_display");
        document.getElementById("user_email").innerHTML = response.message;
        console.debug("hide login show info");
    }
}

function loginToServerCallback(loginToServerResponse) {
    checkLogin();
}

function setupTags() {
    //login button
    const loginButton = document.getElementById(loginButtonID);
    loginButton.addEventListener("click", function (ce) {
        var email = document.getElementById("login_email").value;
        var password = document.getElementById("login_password").value;
        var message = {
            email: email,
            password: password
        };

        var loginMessage = new Messaging(sender, loginMessageType, message);
        console.log(loginMessage);
        chrome.runtime.sendMessage(loginMessage, loginToServerCallback);
    });

//    sync button
    const syncButton = document.getElementById(syncButtonID);
    syncButton.addEventListener("click", function (ce) {
        const syncMessage = new Messaging(sender, syncMessageType, null);
        chrome.runtime.sendMessage(syncMessage, function (response) {
            if (response.messageType === logoutMessageType) {
                checkLogin();
            }
            //todo show message that sync successful.

        });

    });
    const saveTabsButton = document.getElementById(saveTabsButtonID);
    saveTabsButton.addEventListener("click", function (ce) {
        const saveMessage = new Messaging(sender, saveTabsMessageType, null);
        chrome.runtime.sendMessage(saveMessage, function (response) {
            if (response.messageType === logoutMessageType) {
                checkLogin();
            }

        });

    });


//    logout button
    const logoutButton = document.getElementById(logoutButtonID);
    logoutButton.addEventListener("click", function (ce) {
        const logoutMessage = new Messaging(sender, logoutMessageType, null);
        chrome.runtime.sendMessage(logoutMessage, function (response) {
            checkLogin()
        });

    })

}

//end-region


//region {calls}
document.addEventListener("DOMContentLoaded", function (dcle) {
    setupTags();
    checkLogin();

});
//end-region